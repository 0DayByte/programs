#!/usr/bin/env bash
set -x #bash verbose

#########################################################
#
#	Kodi
#	Debian/Ubuntu
#
#########################################################

# Add Repo for libnfs8
touch /etc/apt/sources.list.d/artful.list
echo "deb http://us.archive.ubuntu.com/ubuntu artful main universe" > /etc/apt/sources.list.d/artful.list

# Add Repo
add-apt-repository -y ppa:team-xbmc/unstable
sed -i 's/bionic/artful/g' /etc/apt/sources.list.d/team-xbmc-ubuntu-unstable-bionic.list
mv /etc/apt/sources.list.d/team-xbmc-ubuntu-unstable-bionic.list /etc/apt/sources.list.d/team-xbmc-ubuntu-unstable-artful.list
# For Upgrade
#touch team-xbmc-ubuntu-unstable-artful.list
#echo "deb http://ppa.launchpad.net/team-xbmc/unstable/ubuntu artful main" > /etc/apt/sources.list.d/team-xbmc-ubuntu-unstable-artful.list
apt-get -qq update

# Install Kodi
apt-get -qqy install kodi

rm /etc/apt/sources.list.d/team-xbmc-ubuntu-unstable-artful.list
rm /etc/apt/sources.list.d/artful.list

#EOF