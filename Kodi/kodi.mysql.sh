#!/usr/bin/env bash
set -x #bash verbose

###################################################################
#
#	Kodi MySQL Setup
#	Debian/Ubuntu
#
###################################################################

kodi_pass='PASSWORD'
kodi_user1='user1'
kodi_user2='user2'
mysql_port='3306'

# Install MariaDB
#wget -qO https://gitlabs/.../mariadb.install.deb.sh

# Config MySQL
systemctl stop mariadb
sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i 's/3306/${mysql_port}/g' /etc/mysql/mariadb.conf.d/50-server.cnf
systemctl start mariadb
sleep 5

# Setup databases
#
# Must all databases to be created by kodi
# only give permissions
#
# Name of databases have numbers even though
# numbers are not in the name tag in advancesetting.xml
# videos is 107, music is 70. To find the number look under
# userdata/Databases eg MyVideos107, MyMusic60.
#
# After creating databases rescan media to finish setup

# Create Kodi user enable privileges
# allow kodi to create databases
mysql
CREATE USER 'kodi'@'%' IDENTIFIED BY '${kodi_pass}';
GRANT ALL ON ${user1}_videos107.* TO 'kodi'@'%';
GRANT ALL ON ${user2}_videos107.* TO 'kodi'@'%';
GRANT ALL ON ${user1}_music60.* TO 'kodi'@'%';
GRANT ALL ON ${user2}_music60.* TO 'kodi'@'%';
flush privileges;
quit;

#EOF